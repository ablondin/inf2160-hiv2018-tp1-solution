{-|
Module      : Configuration
Description : A module for representing an induced subtree of a graph
Copyright   : (c) Alexandre Blondin Massé
License     : GPL-3
Maintainer  : blondin_masse.alexandre@uqam.ca
Stability   : experimental

This module provides a data structure called 'Configuration', whose purpose is
to represent an induced subtree. Each vertex of a configuration is either
included, excluded or free.
 -}
module Configuration (
    -- * Type and constructor
    Configuration, initConfiguration,
    -- * Configuration update
    includeVertex, excludeVertex, availableVertex,
    -- * Queries
    subtree, subtreeSize, numLeaves, extendable,
    -- * Graphviz output
    toGraphvizString,
    ) where

import qualified Data.Map.Strict as Map
import Data.List (intercalate, sort)
import Data.Maybe (fromJust)
import Graph hiding (toGraphvizString)
import qualified Graph (toGraphvizString)

------------------------
-- Status of vertices --
------------------------

-- | The possible status of vertices in a configuration:
--
-- * `Include`: the vertex is included in the subtree;
-- * `Excluded`: the vertex is excluded from the subtree;
-- * `Free`: the vertex is avaible for inclusion or exclusion.
data Status = Included | Excluded | Free
    deriving (Eq, Show)

-------------------
-- Configuration --
-------------------

-- | A configuration for enumerating induced subtrees.
data Configuration v = Configuration {
    configurationMap   :: Map.Map v Status,
    configurationGraph :: Graph v
}

instance (Show v, Ord v) => Show (Configuration v) where
    show c@(Configuration vertexToStatus _) = "Configuration("
        ++ "Included={" ++ (showByStatus Included) ++ "}, "
        ++ "Excluded={" ++ (showByStatus Excluded) ++ "}, "
        ++ "Free={" ++ (showByStatus Free) ++ "})"
        where showByStatus s = intercalate "," $ map show $ verticesByStatus s c

-----------------
-- Constructor --
-----------------

-- | Returns a configuration for the given graph
--
-- At first, all vertices have status @Free@.
--
-- >>> initConfiguration $ wheelGraph 4
-- Configuration(Included={}, Excluded={}, Free={0,1,2,3,4})
initConfiguration :: Ord v => Graph v -> Configuration v
initConfiguration g = Configuration (Map.fromList allFree) g
    where allFree = [(u,Free) | u <- vertices g]


-- | Updates the status of the given vertex in a configuration
--
-- >>> updateStatus 1 Included (initConfiguration $ wheelGraph 4)
-- Configuration(Included={1}, Excluded={}, Free={0,2,3,4})
updateStatus :: Ord v => v -> Status -> Configuration v -> Configuration v
updateStatus u s (Configuration m g) = Configuration (Map.insert u s m) g

-- | Includes the given vertex to the configuration
--
-- If the vertex is not available, an error is triggered.
--
-- >>> includeVertex 2 $ initConfiguration $ wheelGraph 4
-- Configuration(Included={2}, Excluded={}, Free={0,1,3,4})
--
-- In some cases, the inclusion of the vertex might trigger the exclusion of
-- other vertices. More precisely, some vertices could not be included anymore
-- without creating a cycle, so that they can safely be excluded.
--
-- >>> let configuration = initConfiguration (wheelGraph 4)
-- >>> includeVertex 2 $ includeVertex 1 $ initConfiguration $ wheelGraph 4
-- Configuration(Included={1,2}, Excluded={0}, Free={3,4})
includeVertex :: Ord v => v -> Configuration v -> Configuration v
includeVertex u c@(Configuration m g)
    | status c u == Free = updateExcluded (filter ((==Free) . status c) (neighbors u g))
                                          (updateStatus u Included c)
    | otherwise          = error "Vertex is not free"
    where updateExcluded :: Ord v => [v] -> Configuration v -> Configuration v
          updateExcluded []     c = c
          updateExcluded (u:us) c = updateExcluded us c'
              where g = configurationGraph c
                    ns = neighbors u g
                    numGreen = length $ filter ((==Included) . status c) ns
                    c' | numGreen <= 1 = c
                       | otherwise     = updateStatus u Excluded c

-- | Excludes the given vertex from the configuration
--
-- If the vertex is not available, an error is triggered.
--
-- >>> excludeVertex 2 $ initConfiguration $ wheelGraph 4
-- Configuration(Included={}, Excluded={2}, Free={0,1,3,4})
excludeVertex :: Ord v => v -> Configuration v -> Configuration v
excludeVertex u c
    | status c u == Free = updateStatus u Excluded c
    | otherwise          = error "Vertex is not free"

-------------
-- Queries --
-------------

-- | Returns the status of a vertex in the configuration
--
-- >>> status (initConfiguration $ completeGraph 4) 1
-- Free
status :: Ord v => Configuration v -> v -> Status
status (Configuration m _) u = fromJust $ Map.lookup u m

-- | Returns all vertices having the given status in the configuration
--
-- >>> verticesByStatus Free $ initConfiguration $ completeGraph 4
-- [1,2,3,4]
verticesByStatus :: Ord v => Status -> Configuration v -> [v]
verticesByStatus s c@(Configuration _ g) = filter ((==s) . status c) (vertices g)

-- | Returns a vertex that is available for either inclusion or exclusion
--
-- Note: For sake of determinism, the returned vertex is always the minimum
-- among all available vertices.
--
-- >>> let configuration = initConfiguration $ completeGraph 4
-- >>> availableVertex configuration
-- Just 1
-- >>> let configuration' = includeVertex 1 $ configuration
-- >>> availableVertex $ configuration'
-- Just 2
-- >>> availableVertex $ includeVertex 2 $ configuration'
-- Nothing
availableVertex :: Ord v => Configuration v -> Maybe v
availableVertex c@(Configuration m g)
    | null candidates = Nothing
    | otherwise       = Just $ minimum candidates
    where noneIncluded   = all (/=Included) (Map.elems m)
          boundary False = concat [neighbors u g | u <- verticesByStatus Included c]
          boundary True  = verticesByStatus Free c
          candidates     = filter ((==Free) . status c) (boundary noneIncluded)

-- | Returns True if the configuration is extendable.
--
-- A configuration is extendable if it has a free vertex available.
--
-- >>> extendable $ initConfiguration $ completeGraph 4
-- True
extendable :: Ord v => Configuration v -> Bool
extendable c = availableVertex c /= Nothing

-- | Returns the vertices forming the subtree represented by the configuration
--
-- >>> subtree $ includeVertex 2 $ includeVertex 1 $ initConfiguration $ cycleGraph 4
-- [1,2]
-- >>> subtree $ includeVertex 1 $ includeVertex 2 $ initConfiguration $ cycleGraph 4
-- [1,2]
subtree :: Ord v => Configuration v -> [v]
subtree = sort . verticesByStatus Included

-- | Returns the size of the induced subtree represented by the configuration
--
-- >>> subtreeSize $ initConfiguration $ completeGraph 4
-- 0
-- >>> subtreeSize $ includeVertex 2 $ includeVertex 1 $ initConfiguration $ cycleGraph 4
-- 2
subtreeSize :: Ord v => Configuration v -> Int
subtreeSize = length . subtree

-- | Indicates whether a given vertex is a leaf in the subtree represented by
-- the configuration
--
-- >>> let c = includeVertex 2 $ includeVertex 1 $ initConfiguration $ cycleGraph 4
-- >>> [isLeaf c i | i <- [1..4]]
-- [True,True,False,False]
isLeaf :: Ord v => Configuration v -> v -> Bool
isLeaf c@(Configuration m g) u = status c u == Included
    && (length $ filter ((==Included) . status c) (neighbors u g)) == 1

-- | Returns the number of leaves in the subtree represented by the
-- configuration
--
-- >>> let c = includeVertex 2 $ includeVertex 1 $ initConfiguration $ cycleGraph 4
-- >>> numLeaves c
-- 2
numLeaves :: Ord v => Configuration v -> Int
numLeaves c = length $ filter (isLeaf c) $ verticesByStatus Included c

--------------
-- Graphviz --
--------------

-- | Returns a Graphviz string for a given node in the configuration
--
-- >>> nodeToGraphvizString (initConfiguration $ completeGraph 4) 1
-- "\"1\" [label=\"1\", fillcolor=white];"
nodeToGraphvizString :: (Ord v, Show v) => Configuration v -> v -> String
nodeToGraphvizString c u = str ++ " [label=" ++ str ++ ", fillcolor=" ++ color st ++ "];"
    where str            = (show . show) u
          st             = status c u
          color Included = "green"
          color Excluded = "red"
          color Free     = "white"

-- | Returns a Graphviz string for a given edge in the configuration
--
-- >>> edgeToGraphvizString (initConfiguration $ completeGraph 4) (1,2)
-- "\"1\" -- \"2\";"
edgeToGraphvizString :: (Ord v, Show v) => Configuration v -> (v,v) -> String
edgeToGraphvizString c (u,w) = show2 u ++ " -- " ++ show2 w ++ ";"
    where show2 = show . show

-- | Returns a Graphviz string for the given configuration
--
-- >>> let configuration = includeVertex 2 $ includeVertex 1 $ initConfiguration $ wheelGraph 5
-- >>> putStrLn $ toGraphvizString configuration
-- graph {
--   margin=0;
--   node [fixedsize=true, width=0.3, height=0.3, style=filled];
--   "0" [label="0", fillcolor=red];
--   "1" [label="1", fillcolor=green];
--   "2" [label="2", fillcolor=green];
--   "3" [label="3", fillcolor=white];
--   "4" [label="4", fillcolor=white];
--   "5" [label="5", fillcolor=white];
--   "0" -- "5";
-- ...
--   "4" -- "5";
-- }
toGraphvizString :: (Ord v, Show v) => Configuration v -> String
toGraphvizString c@(Configuration m g) = intercalate "\n" [headerString, verticesString, edgeString, footerString]
    where headerString = "graph {\n" ++ "  margin=0;\n  node [fixedsize=true, width=0.3, height=0.3, style=filled];";
          footerString = "}"
          verticesString = intercalate "\n" $ map (("  "++) . nodeToGraphvizString c) (vertices g)
          edgeString = intercalate "\n" $ map (("  "++) . edgeToGraphvizString c) (edges g)
